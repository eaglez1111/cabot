//  Eagle Zhao, tim.eagle.zhao@gmail.com or dapengz@andrew
// 19 Apr 2019

#include "quaternionFilters.h"
#include "MyMPU9250.h"

#include <ros.h>
#include <cabot_msgs/ArduinoSensors.h>

#include <Wire.h>
#include <TimerOne.h>

#define    MPU9250_ADDRESS            0x68
#define    MAG_ADDRESS                0x0C

#define    GYRO_FULL_SCALE_250_DPS    0x00
#define    GYRO_FULL_SCALE_500_DPS    0x08
#define    GYRO_FULL_SCALE_1000_DPS   0x10
#define    GYRO_FULL_SCALE_2000_DPS   0x18

#define    ACC_FULL_SCALE_2_G        0x00
#define    ACC_FULL_SCALE_4_G        0x08
#define    ACC_FULL_SCALE_8_G        0x10
#define    ACC_FULL_SCALE_16_G       0x18

#define rate 100 //hz
#define TorquePin A0
#define ForcePin A1
ros::NodeHandle  nh;
cabot_msgs::ArduinoSensors as;
ros::Publisher Arduino_topic("ArduinoGPIO_topic", &as);

unsigned long tEOP;
int16_t ax,ay,az,gx,gy,gz,mx,my,mz;

void setup()
{
  nh.initNode();
  nh.advertise(Arduino_topic);
  tEOP=millis();
  IMU_init();

  nh.loginfo("Init done!");
}


void loop()
{
  // delay
  while(tEOP>millis()){
    updateIMUVals(as.ax,as.ay,as.az,
                  as.gx,as.gy,as.gz,
                  as.mx,as.my,as.mz);
  }
  tEOP+=1000.0/rate;

  // get imu data
  as.header.stamp = nh.now();

  // get gpio data
  updateTFVals(as.torque,as.force);

  // Publish
  Arduino_topic.publish(&as);
  nh.spinOnce();
}
//nh.logdebug loginfo logwarn logerror logfatal
//dtostrf(3.14,7,3,strf);  sprintf(log_msg,"points[0].positions[0] = %s",strf);


void printFloat(float a){
  char strf[12];
  dtostrf(a,11,4,strf);
  nh.loginfo(strf);
}

void updateAnalogVals(uint16_t v[]){
  for(int i=A0;i<=A7;i++){
    v[i-A0]=analogRead(i);
  }
}

void updateTFVals(uint16_t &t,uint16_t &f){
  t=analogRead(TorquePin);
  f=analogRead(ForcePin);
}

void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data) {
  Wire.beginTransmission(Address); // Set register address
  Wire.write(Register);
  Wire.endTransmission();
  Wire.requestFrom(Address, Nbytes); // Read Nbytes
  uint8_t index=0;
  while (Wire.available())
  Data[index++]=Wire.read();
}

// Write a byte (Data) in device (Address) at register (Register)
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data) {
  Wire.beginTransmission(Address);  // Set register address
  Wire.write(Register);
  Wire.write(Data);
  Wire.endTransmission();
}



void IMU_init(){
  Wire.begin();
  I2CwriteByte(MPU9250_ADDRESS,29,0x06); // Set accelerometers low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,26,0x06); // Set gyroscope low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,27,GYRO_FULL_SCALE_1000_DPS); // Configure gyroscope range
  I2CwriteByte(MPU9250_ADDRESS,28,ACC_FULL_SCALE_4_G); // Configure accelerometers range
  I2CwriteByte(MPU9250_ADDRESS,0x37,0x02); // Set by pass mode for the magnetometers
  I2CwriteByte(MAG_ADDRESS,0x0A,0x16);  // Request continuous magnetometer measurements in 16 bits
  Timer1.initialize(10000); // initialize timer1, and set a 1/2 second period
}


void updateIMUVals(int16_t &ax, int16_t &ay, int16_t &az,
                    int16_t &gx, int16_t &gy, int16_t &gz,
                    int16_t &mx, int16_t &my, int16_t &mz) {
  // :::  accelerometer and gyroscope :::

  // Read accelerometer and gyroscope
  uint8_t Buf[14];
  I2Cread(MPU9250_ADDRESS,0x3B,14,Buf);

  // Create 16 bits values from 8 bits data

  // Accelerometer
  ax=-(Buf[0]<<8 | Buf[1]);
  ay=-(Buf[2]<<8 | Buf[3]);
  az=Buf[4]<<8 | Buf[5];

  // Gyroscope

  gx=-(Buf[8]<<8 | Buf[9]);
  gy=-(Buf[10]<<8 | Buf[11]);
  gz=Buf[12]<<8 | Buf[13];


  // _____________________
  // :::  Magnetometer :::
  // Read register Status 1 and wait for the DRDY: Data Ready
  uint8_t ST1;
  do
  {
    I2Cread(MAG_ADDRESS,0x02,1,&ST1);
  }
  while (!(ST1&0x01));

  // Read magnetometer data
  uint8_t Mag[7];
  I2Cread(MAG_ADDRESS,0x03,7,Mag);
  // Create 16 bits values from 8 bits data
  // Magnetometer
  mx=-(Mag[3]<<8 | Mag[2]);
  my=-(Mag[1]<<8 | Mag[0]);
  mz=-(Mag[5]<<8 | Mag[4]);

}
