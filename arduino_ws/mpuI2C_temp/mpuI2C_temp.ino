// SDA A4, SCL A5

#include "quaternionFilters.h"
#include "MPU9250.h"

#define MPU9250_ADDRESS MPU9250_ADDRESS_AD0
MPU9250 myIMU(MPU9250_ADDRESS, Wire);


void setup()
{
  Serial.begin(115200);
  IMU_init();
}

void loop()
{
  float _y;
  updateIMUVals(_y);
  Serial.println(_y, 2);

}

void IMU_init(){
  Wire.begin(); // TWBR = 12;  // 400 kbit/sec I2C speed

  float _bias[3];
  myIMU.calibrateMPU9250(_bias, myIMU.accelBias);
  myIMU.initMPU9250();
  myIMU.initAK8963(myIMU.factoryMagCalibration);
}

void updateIMUVals(float &y){
  float ax, ay, az, gx, gy, gz, mx, my, mz;
  if (myIMU.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)
  {
    int16_t gyroCount[3];
    int16_t magCount[3];
    int16_t accelCount[3];

    myIMU.readAccelData(accelCount);  // Read the x/y/z adc values
    ax = (float)accelCount[0] * aRes - myIMU.accelBias[0];
    ay = (float)accelCount[1] * aRes - myIMU.accelBias[1];
    az = (float)accelCount[2] * aRes - myIMU.accelBias[2];
    myIMU.readGyroData(gyroCount);  // Read the x/y/z adc values

    gx = (float)gyroCount[0] * gRes;
    gy = (float)gyroCount[1] * gRes;
    gz = (float)gyroCount[2] * gRes;
    myIMU.readMagData(magCount);  // Read the x/y/z adc values

    mx = (float)magCount[0] * mRes * myIMU.factoryMagCalibration[0] - (147.07);//myIMU.magBias[0];
    my = (float)magCount[1] * mRes * myIMU.factoryMagCalibration[1] - (390.48);//myIMU.magBias[1];
    mz = (float)magCount[2] * mRes * myIMU.factoryMagCalibration[2] - (-63.04);//myIMU.magBias[2];
  }

  myIMU.updateTime();
  MahonyQuaternionUpdate(ax, ay, az, gx * DEG_TO_RAD,
                         gy * DEG_TO_RAD, gz * DEG_TO_RAD, my,
                         mx, mz, myIMU.deltat);

  y   = atan2(2.0f * (*(getQ()+1) * *(getQ()+2) + *getQ()
                * *(getQ()+3)), *getQ() * *getQ() + *(getQ()+1)
                * *(getQ()+1) - *(getQ()+2) * *(getQ()+2) - *(getQ()+3)
                * *(getQ()+3));

  y   *= RAD_TO_DEG;
}
