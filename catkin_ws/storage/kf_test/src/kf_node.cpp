// to test kf()
// EagleZ, tim.eagle.zhao@gmail.com or dapengz@andrew

#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "cabot_msgs/ArduinoSensors.h"
#include "cabot_msgs/kf_yaws.h"
#include <iostream>

#define pi_ 3.14159265358979323846
#define deg2rad (pi_/180.0)
#define A 1 //no need to change
#define H 1 //no need to change
#define kf_p 1.0 // Covariance of the filtered result
#define kf_q 1.0 // Covariance of prediction
#define kf_r 1500.0 // Covariance of observation, idealy only need to adjust this value


float adaptAngle(float b, float a, int bound=2, int mode=0){ //mode0-rad 1-deg ,
  if(mode==0){
    b/=deg2rad;
    a/=deg2rad;
  }
  float b0=b,result;
  float diff_min,diff;
  diff_min=360*(bound+1);

  for (float b=b0-bound*360; b<=b0+bound*360; b+=360){
    diff=b-a;
    if(diff<0) diff=-diff;
    if(diff<diff_min) {
      result=b;
      diff_min=diff;
    }
  }
  if(mode==0){
    result*=deg2rad;
  }
  return result;
}

float kf(float z){
  static float p=kf_p, x=0;
  float K;
  static int flag_firstTime=1;

  if (flag_firstTime){
    x=z;
    flag_firstTime=0;
  }
  else{
    z=adaptAngle(z,x); //to avoid 2pi jump between incoming z and previous x
    //x=A*x;  //prediction
    p=p+kf_q; // A*p*A+q;
    K=p/(p+kf_r); // p*H/(h*p*H+R)
    x=K*z+(1-K)*x; // x+K*(z-H*x) //fuse prediction and observation
    p=(1-K)*p;//(1-K*H)*p;
  }

  return x;
}

ros::Publisher pub;
ros::Subscriber sub;

void yawCallback(const cabot_msgs::ArduinoSensors::ConstPtr& input)
{
  cabot_msgs::kf_yaws output;
  output.yawRaw=input->yaw;
  output.yawRaw_deg=output.yawRaw/deg2rad;
  output.yaw=kf(input->yaw);
  output.yaw_deg=output.yaw/deg2rad;
  pub.publish(output);
  ROS_INFO("yawRaw:[%.2f,%.2f], yawFiltered:[%.2f,%.2f]", input->yaw, output.yawRaw_deg, output.yaw,output.yaw_deg);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "kf_node");
  ros::NodeHandle n;
  pub = n.advertise<cabot_msgs::kf_yaws>("kf_output", 1000);
  sub = n.subscribe("ArduinoSensors_topic", 3, yawCallback);

  while(1){
    ros::spinOnce();
  }
  return 0;
}
