#!/usr/bin/env python

# EagleZ, tim.eagle.zhao@gmail.com or dapengz@andrew
# Apr 2019

'''ROS Imports'''
import rospy
from cabot_msgs.msg import MotorStatus
from cabot_msgs.msg import MotorTarget
from std_msgs.msg import Header


'''Functional Imports'''
import serial
import time
import numpy as np


'''Parameter'''
wheel_diam = 0.083 # meter
mpc = wheel_diam*np.pi/84 # meter per count = wheel circumference / encoder counts_per_turn
freq=100 #Hz
printCtrl=0 # print status information or not
leftIs1=1 # left is axis1, right is axis0
signLeft=1.0
signRight=-1.0
SerialPort='/dev/ttyACM0'
serialReading_timeout=0.25 #sec


'''Functional Constants (DON'T CHANGE)'''
AXIS_STATE_IDLE = 1
AXIS_STATE_CLOSED_LOOP_CONTROL = 8


'''Global Varaible'''
spd0_c, spd1_c = 0, 0
loopCtrl_on = 1

'''Hardware Initialization'''
try:
    od=serial.Serial(SerialPort,115200,timeout=serialReading_timeout)
except:
    print "Check Odrive connection:", SerialPort, "doesn't exsist! "
else:
    print "Odrve connected as ",od.name," !"


'''Subscriber Routine'''
def MotorTargetRoutine(data):
    global spd0_c
    global spd1_c
    global loopCtrl_on

    loopCtrl_on = data.loopCtrl

    spd0_c = signLeft*data.spdLeft/mpc
    spd1_c = signRight*data.spdRight/mpc
    if leftIs1:
        spd0_c, spd1_c = spd1_c, spd0_c

    print "I heard:  ", data.loopCtrl, "(loopCtrl): ", data.spdLeft, data.spdRight, "(m/s)"
    print "I'm doing:", spd0_c, spd1_c, "(m/s)"


'''Main()'''
def main():
    pub = rospy.Publisher('motorStatus', MotorStatus, queue_size=10)
    rospy.init_node('odrive_node', anonymous=True)
    rospy.Subscriber('motorTarget', MotorTarget, MotorTargetRoutine)
    rate = rospy.Rate(freq)

    ms = MotorStatus()

    mode_written=1
    spd0_c_written,spd1_c_written=0,0

    while not rospy.is_shutdown():

        # send new velocity command
        if(mode_written!=loopCtrl_on):
            print 'w m'
            if od_writeMode(loopCtrl_on):
                mode_written=loopCtrl_on

        if(spd0_c_written!=spd0_c):
            print 'w 0'
            if od_writeSpd(0,spd0_c):
                spd0_c_written=spd0_c

        if(spd1_c_written!=spd1_c):
            print 'w 1'
            if od_writeSpd(1,spd1_c):
                spd1_c_written=spd1_c

        # update encoder counts and speed
        enc0 = getResponse("r axis0.encoder.pos_estimate")
        enc1 = getResponse("r axis1.encoder.pos_estimate")
        spd0 = getResponse("r axis0.encoder.vel_estimate")
        spd1 = getResponse("r axis1.encoder.vel_estimate")

        ms.header.stamp = rospy.Time.now()

        ms.distLeft_c, ms.distRight_c = enc0, enc1
        ms.spdLeft_c, ms.spdRight_c = spd0, spd1

        if leftIs1:
            ms.distLeft_c, ms.distRight_c = ms.distRight_c, ms.distLeft_c
            ms.spdLeft_c, ms.spdRight_c = ms.spdRight_c, ms.spdLeft_c

        ms.distLeft_c,ms.distRight_c = signLeft*ms.distLeft_c, signRight*ms.distRight_c
        ms.spdLeft_c,ms.spdRight_c = signLeft*ms.spdLeft_c, signRight*ms.spdRight_c

        ms.distLeft, ms.distRight = ms.distLeft_c*mpc, ms.distRight_c*mpc
        ms.spdLeft, ms.spdRight = ms.spdLeft_c*mpc, ms.spdRight_c*mpc

        pub.publish(ms)
        rate.sleep()


def od_writeSpd(ch,spd):
    spd_s=str(spd)[:7]
    ch=int(ch!=0)
    cmd='v '+str(ch)+' '+spd_s
    return od_write(cmd,len(cmd)+1)


def od_writeMode(loopCtrl_on): # For more information about the number in command: https://github.com/madcowswe/ODrive/blob/master/tools/odrive/enums.py OR contact EagleZ
    if (loopCtrl_on==1):
        return (od_write('w axis0.requested_state 8',26) and od_write('w axis1.requested_state 8',26))
    else:
        return (od_write('w axis0.requested_state 1',26) and od_write('w axis1.requested_state 1',26))


def od_write(st,length): # To be improved
    try:
        if (od.write(st+'\n')==length):
            return 1
    except:
        print "FAILED!!!! "+str(st)
    return 0


def getResponse(st): # To be improved
    od_write(st,29)
    try:
        return float(od.readline())
    except:
        print "FAILED!!!! "+str(st)
        return 0.0

def od_reboot():
    od_write('sb',3)


'''Run'''
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass




#
