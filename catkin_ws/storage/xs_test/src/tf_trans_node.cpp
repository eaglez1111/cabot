// to test kf()
// EagleZ, tim.eagle.zhao@gmail.com or dapengz@andrew

#include "ros/ros.h"
#include <iostream>


#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/convert.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/Imu.h>



//ros::Publisher pub;
ros::Subscriber sub;

void imuCallback(const sensor_msgs::Imu::ConstPtr& input)
{
  static tf2_ros::TransformBroadcaster pub;
  geometry_msgs::TransformStamped output;

  output.header.stamp = ros::Time::now();
  output.header.frame_id = "tf_trans_ez0";
  output.child_frame_id = "tf_trans_ez1";

  output.transform.rotation = input->orientation;
  /*output.transform.rotation.x = input.orientation.x();
  output.transform.rotation.y = q_.y();
  output.transform.rotation.z = q_.z();
  output.transform.rotation.w = q_.w();*/
  pub.sendTransform(output);

}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "tf_trans_node");
  ros::NodeHandle n;

  sub = n.subscribe("imu/data", 5, imuCallback);

  while(1){
    ros::spinOnce();
  }
  return 0;
}
